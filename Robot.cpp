#include "Angel.h"
#include "Robot.h"
#include <unistd.h>
#include <iostream>

typedef Angel::vec4  color4;
typedef Angel::vec4  point4;

//---------------------------------------------------------------------------
//RGBA Colors
color4 vertex_colors[8] = {
    color4( 0.643, 0.776, 0.224, 1.0 ),  // android green #a4c639
    color4( 0.3, 0.3, 0.3, 1.0 ),  // black
    color4( 1.0, 0.2, 0.2, 1.0 ),  // red
    color4( 1.0, 1.0, 0.2, 1.0 ),  // yellow
    color4( 0.2, 0.2, 1.0, 1.0 ),  // blue
    color4( 1.0, 0.2, 1.0, 1.0 ),  // magenta
    color4( 1.0, 1.0, 1.0, 1.0 ),  // white
    color4( 0.2, 1.0, 1.0, 1.0 )   // cyan
};

class Limb {
public:
    void Draw();
    GLint color_index;
    color4 material_ambient;
    color4 material_diffuse;
    color4 material_specular;
    mat4 crm, cr_t, cr_r, cr_s;
    mat4 pivot;
    mat4 parentcrm;
    int child = 10;
};

void Limb::Draw( ) {
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(points)) );
    
    glUniform4fv( ka, 1, material_ambient );
    glUniform4fv( kd, 1, material_diffuse );
    glUniform4fv( ks, 1, material_specular );
    
    glUniformMatrix4fv( model, 1, GL_TRUE, crm );
    
    glDrawArrays( GL_TRIANGLES, 0, NumVertices );
}

class Head {
public:
    void Draw();
    GLint color_index;
    color4 material_ambient;
    color4 material_diffuse;
    color4 material_specular;
    mat4 crm, cr_t, cr_r, cr_s;
    mat4 pivot;
    mat4 parentcrm;
    int child = 10;
};

void Head::Draw() {
    glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(points)+sizeof(normals)));
    glVertexAttribPointer(vNormal, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(sdata)));
    
    glUniform4fv( ka, 1, material_ambient );
    glUniform4fv( kd, 1, material_diffuse );
    glUniform4fv( ks, 1, material_specular );
    
    glUniformMatrix4fv( model, 1, GL_TRUE, crm );
    
    glDrawArrays( GL_TRIANGLES, 0, NumSphereVertices );
}

class Circle {
public:
    void Draw();
    GLint color_index;
    color4 material_ambient;
    color4 material_diffuse;
    color4 material_specular;
    mat4 crm, cr_t, cr_r, cr_s;
    mat4 pivot;
    mat4 parentcrm;
    int child = 10;
};

void Circle::Draw() {
    glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(points)+sizeof(normals)+ sizeof(sdata)+ sizeof(snormals)));
    glVertexAttribPointer(vNormal, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(sdata)+ sizeof(snormals)+ sizeof(cdata)));
    
    glUniform4fv( ka, 1, material_ambient );
    glUniform4fv( kd, 1, material_diffuse );
    glUniform4fv( ks, 1, material_specular );
    
    glUniformMatrix4fv( model, 1, GL_TRUE, crm );
    
    glDrawArrays( GL_TRIANGLES, 0, 240 );
}


//----- RubiksCube models the entire Rubik's cube

class Robot {
public:
    Robot();
    void Draw();
    void SetCubesRotate(int* c) {for(int i=0; i<4; i++) cubes_to_rotate[i] = c[i];};
private:
    Head head;
    Circle circle;
    Limb cubes[8];
    int cubes_to_rotate[4];
};

Robot::Robot() {
    mat4 origin = Translate(0.0, 0.0, 0.0);
    //Head NEEDS TO BE HALF SPHERE
    head.color_index = 0;
    head.cr_t = Translate(0.0, 4.5, 0.0);
    head.cr_s = Scale(1.0, 1.0, 1.0);
    head.pivot = Translate(0.0, 0.0, 0.0);
    head.parentcrm = origin;
    
    //Body ALL BODY PARTS TO BE CYLINDERS
    cubes[BODY].color_index = 0;
    cubes[BODY].cr_t = Translate(0.0, 3.0, 0.0);
    cubes[BODY].cr_s = Scale(1.5, 2.0, 1.0);
    cubes[BODY].pivot = Translate(0.0, 0.0, 0.0);
    cubes[BODY].parentcrm = origin;
    
    //Arm R
    cubes[RIGHT_ARM].color_index = 0;
    cubes[RIGHT_ARM].cr_t = Translate(-1.0, 3.50, 0.0);
    cubes[RIGHT_ARM].cr_s = Scale(0.5, 2.0, 1.0);
    cubes[RIGHT_ARM].pivot = Translate(0.0, -0.5, 0.0);
    cubes[RIGHT_ARM].parentcrm = origin;
    //Upper Leg R
    cubes[RIGHT_UPPER_LEG].color_index = 0;
    cubes[RIGHT_UPPER_LEG].cr_t = Translate(-0.5, 2.0, 0.0);
    cubes[RIGHT_UPPER_LEG].cr_s = Scale(0.5, 1.0, 1.0);
    cubes[RIGHT_UPPER_LEG].pivot = Translate(0.0, -0.5, 0.0);
    cubes[RIGHT_UPPER_LEG].parentcrm = origin;
    cubes[RIGHT_UPPER_LEG].child = RIGHT_LOWER_LEG;
    //Lower Leg R
    cubes[RIGHT_LOWER_LEG].color_index = 0;
    cubes[RIGHT_LOWER_LEG].cr_t = Translate(0.0, -1.0, 0.0);
    cubes[RIGHT_LOWER_LEG].cr_s = Scale(0.5, 1.0, 1.0);
    cubes[RIGHT_LOWER_LEG].pivot = Translate(0.0, -0.5, 0.0);
    cubes[RIGHT_LOWER_LEG].parentcrm = cubes[RIGHT_UPPER_LEG].cr_t * cubes[RIGHT_UPPER_LEG].cr_r;
    
    //Arm L
    cubes[LEFT_ARM].color_index = 0;
    cubes[LEFT_ARM].cr_t = Translate(1.0, 3.50, 0.0);
    cubes[LEFT_ARM].cr_s = Scale(0.5, 2.0, 1.0);
    cubes[LEFT_ARM].pivot = Translate(0.0, -0.50, 0.0);
    cubes[LEFT_ARM].parentcrm = origin;
    //Upper Leg L
    cubes[LEFT_UPPER_LEG].color_index = 0;
    cubes[LEFT_UPPER_LEG].cr_t = Translate(0.5, 2.0, 0.0);
    cubes[LEFT_UPPER_LEG].cr_s = Scale(0.5, 1.0, 1.0);
    cubes[LEFT_UPPER_LEG].pivot = Translate(0.0, -0.5, 0.0);
    cubes[LEFT_UPPER_LEG].parentcrm = origin;
    cubes[LEFT_UPPER_LEG].child = LEFT_LOWER_LEG;
    //Lower Leg L
    cubes[LEFT_LOWER_LEG].color_index = 0;
    cubes[LEFT_LOWER_LEG].cr_t = Translate(0.0, -1.0, 0.0);
    cubes[LEFT_LOWER_LEG].cr_s = Scale(0.5, 1.0, 1.0);
    cubes[LEFT_LOWER_LEG].pivot = Translate(0.0, -0.5, 0.0);
    cubes[LEFT_LOWER_LEG].parentcrm = cubes[LEFT_UPPER_LEG].cr_t * cubes[LEFT_UPPER_LEG].cr_r;
    
    //GROUND
    cubes[GROUND].color_index = 1;
    cubes[GROUND].cr_t = Translate(1.0, -0.1, 1.0);
    cubes[GROUND].cr_s = Scale(10.0, 0.2, 10.0);
    cubes[GROUND].pivot = Translate(0.0, 0.0, 0.0);
    cubes[GROUND].parentcrm = origin;
    
    
    circle.color_index = 0;
    circle.cr_t = Translate(0.0, .5, 0.0);
    circle.cr_s = Scale(1.0, 1.0, 1.0);
    circle.pivot = Translate(0.0, 0.0, 0.0);
    circle.parentcrm = origin;
    for (int i = 0; i < 8; i++)
    {
        cubes[i].material_ambient = { 0.6, 0.6, 0.6, 1.0 };
        cubes[i].material_ambient *= vertex_colors[cubes[i].color_index];
        cubes[i].material_diffuse = { 0.9, 0.9, 0.9, 1.0 };
        cubes[i].material_diffuse *= vertex_colors[cubes[i].color_index];
        cubes[i].material_specular = { 1.0, 1.0, 1.0, 1.0 };
        cubes[i].material_specular *= vertex_colors[cubes[i].color_index];
        cubes[i].cr_r = (RotateX(0)*RotateY(0)*RotateZ(0 ));
        cubes[i].crm = cubes[i].parentcrm * cubes[i].cr_t * (cubes[i].cr_r*cubes[i].pivot) * cubes[i].cr_s;
    }
    
    head.material_ambient = { 0.6, 0.6, 0.6, 1.0 };
    head.material_ambient *= vertex_colors[head.color_index];
    head.material_diffuse = { 0.9, 0.9, 0.9, 1.0 };
    head.material_diffuse *= vertex_colors[head.color_index];
    head.material_specular = { 1.0, 1.0, 1.0, 1.0 };
    head.material_specular *= vertex_colors[head.color_index];
    head.cr_r = (RotateX(0)*RotateY(0)*RotateZ(0 ));
    head.crm = head.parentcrm * head.cr_t * (head.cr_r*head.pivot) * head.cr_s;
    
    circle.material_ambient = { 0.6, 0.6, 0.6, 1.0 };
    circle.material_ambient *= vertex_colors[circle.color_index];
    circle.material_diffuse = { 0.9, 0.9, 0.9, 1.0 };
    circle.material_diffuse *= vertex_colors[circle.color_index];
    circle.material_specular = { 1.0, 1.0, 1.0, 1.0 };
    circle.material_specular *= vertex_colors[circle.color_index];
    circle.cr_r = (RotateX(0)*RotateY(0)*RotateZ(0 ));
    circle.crm = circle.parentcrm * circle.cr_t * (circle.cr_r*circle.pivot) * circle.cr_s;
    
}

void Robot::Draw() {
    int k = 0;
    head.crm = head.parentcrm * head.cr_t * (head.cr_r*head.pivot) * head.cr_s;
    head.Draw();
    
    circle.Draw();
    
    for (int i = 0; i < 8; i++)
    {
        if ( i == cubes_to_rotate[k])
        {
            k++;
            mat4  m = ( RotateX( Theta[Xaxis] ) * RotateY( Theta[Yaxis] ) * RotateZ( Theta[Zaxis] ) );
            cubes[i].cr_r = m * cubes[i].cr_r;
            cubes[i].crm = cubes[i].parentcrm * cubes[i].cr_t * (cubes[i].cr_r*cubes[i].pivot) * cubes[i].cr_s;
        }
        
        cubes[i].Draw();
        if (cubes[i].child != 10) {
            int j = cubes[i].child;
            cubes[j].crm = cubes[j].parentcrm * cubes[j].cr_t * (cubes[j].cr_r*cubes[j].pivot) * cubes[j].cr_s;
            cubes[j].parentcrm =  cubes[i].cr_t * cubes[i].cr_r;
        }
    }
    usleep(5000);
}
