#include "Angel.h"
#include "Robot.h"
#include <unistd.h>
#include <iostream>

typedef Angel::vec4  color4;
typedef Angel::vec4  point4;

// Lighting
GLuint ka, kd, ks;

// Model transformation parameters
int Axis = Xaxis;
GLfloat Theta[NumAxes] = { 0.0, 0.0, 0.0 };
GLfloat theta_total = 0.0;
int dirn = 1;
bool do_rotation = false;

GLuint  model;  // shader variable location
GLuint vColor;
GLuint vPosition;
GLuint vNormal;

// Viewing transformation parameters
GLfloat Cx = 1.0;
GLfloat Cy = 8.0;
GLfloat Cz = 8.0;
point4  eye( Cx, Cy, Cz, 1.0 );
point4  at( 0.0, 0.0, 0.0, 1.0 );
vec4    up( 0.0, 1.0, 0.0, 0.0 );

GLfloat  left = -1.0, right = 1.0;
GLfloat  bottom = -1.0, top = 1.0;
GLfloat  zNear = 1.5, zFar = 100.0;
// Projection transformation parameters
GLuint  projection; // projection matrix uniform shader variable location
GLuint  view;  // shader variable location

//---------------------------------------------------------------------------
//-----  Texture mapping

GLuint textures[3];
GLuint loadBMP_custom(unsigned char** data, int* w, int* h, const char * imagepath);

//---------------------------------------------------------------------------
//RGBA Colors
color4 vertex_colors[8] = {
    color4( 0.643, 0.776, 0.224, 1.0 ),  // android green #a4c639
    color4( 0.3, 0.3, 0.3, 1.0 ),  // black
    color4( 1.0, 0.2, 1.0, 1.0 ),  // magenta
    color4( 1.0, 0.2, 0.2, 1.0 ),  // red
    color4( 1.0, 1.0, 0.2, 1.0 ),  // yellow
    color4( 0.2, 0.2, 1.0, 1.0 ),  // blue
    color4( 1.0, 1.0, 1.0, 1.0 ),  // white
    color4( 0.2, 1.0, 1.0, 1.0 )   // cyan
};

class Cylinder {
public:
    void Draw();
    GLint color_index;
    color4 material_ambient;
    color4 material_diffuse;
    color4 material_specular;
    mat4 crm, cr_t, cr_r, cr_s;
    mat4 pivot;
    mat4 parentcrm;
    int child = 10;
};

void Cylinder::Draw() {
    glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(points)+sizeof(normals)+ sizeof(sdata)+ sizeof(snormals)));
    glVertexAttribPointer(vNormal, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(sdata)+ sizeof(snormals)+ sizeof(cdata)));
    
    glUniform4fv( ka, 1, material_ambient );
    glUniform4fv( kd, 1, material_diffuse );
    glUniform4fv( ks, 1, material_specular );
    
    glUniformMatrix4fv( model, 1, GL_TRUE, crm );
    
    glDrawArrays( GL_TRIANGLES, 0, NumSides * 12 );
}

class Cube {
public:
    void Draw();
    GLint color_index;
    color4 material_ambient;
    color4 material_diffuse;
    color4 material_specular;
    mat4 crm, cr_t, cr_r, cr_s;
    mat4 pivot;
    mat4 parentcrm;
    int child = 10;
};

void Cube::Draw( ) {
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(points)) );
    
    glUniform4fv( ka, 1, material_ambient );
    glUniform4fv( kd, 1, material_diffuse );
    glUniform4fv( ks, 1, material_specular );
    
    glUniformMatrix4fv( model, 1, GL_TRUE, crm );
    
    glDrawArrays( GL_TRIANGLES, 0, NumVertices );
}

class Sphere {
public:
    void Draw();
    GLint color_index;
    color4 material_ambient;
    color4 material_diffuse;
    color4 material_specular;
    mat4 crm, cr_t, cr_r, cr_s;
    mat4 pivot;
    mat4 parentcrm;
    Cylinder child[4];
};

void Sphere::Draw() {
    glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(points)+sizeof(normals)));
    glVertexAttribPointer(vNormal, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(sdata)));
    
    glUniform4fv( ka, 1, material_ambient );
    glUniform4fv( kd, 1, material_diffuse );
    glUniform4fv( ks, 1, material_specular );
    
    glUniformMatrix4fv( model, 1, GL_TRUE, crm );
    
    glDrawArrays( GL_TRIANGLES, 0, NumSphereVertices );
}

//----- RubiksCube models the entire Rubik's cube

class Robot {
public:
    Robot();
    void Draw();
    void SetCubesRotate(int* c) {for(int i=0; i<4; i++) cubes_to_rotate[i] = c[i];};
private:
    Sphere head;
    Cylinder limbs[8];
    Sphere joints[4];
    int cubes_to_rotate[4];
};

Robot::Robot() {
    mat4 origin = Translate(0.0, 0.0, 0.0);
    //Head NEEDS TO BE HALF SPHERE
    head.color_index = 0;
    head.cr_t = Translate(0.0, 4.1, 0.0);
    head.cr_s = Scale(1.0, 1.0, 1.0);
    head.pivot = Translate(0.0, 0.0, 0.0);
    head.parentcrm = origin;
    
    //Antenna R
    head.child[RIGHT_ANTENNA].color_index = 0;
    head.child[RIGHT_ANTENNA].cr_t = Translate(-0.55, 0.75, 0.0);
    head.child[RIGHT_ANTENNA].cr_s = Scale(0.1, 1.5, 0.1);
    head.child[RIGHT_ANTENNA].pivot = Translate(0.0, 0.0, 0.0);
    head.child[RIGHT_ANTENNA].cr_r = (RotateX(0)*RotateY(0)*RotateZ(45));
    
    //Antenna L
    head.child[LEFT_ANTENNA].color_index = 0;
    head.child[LEFT_ANTENNA].cr_t = Translate(0.55, 0.75, 0.0);
    head.child[LEFT_ANTENNA].cr_s = Scale(0.1, 1.5, 0.1);
    head.child[LEFT_ANTENNA].pivot = Translate(0.0, 0.0, 0.0);
    head.child[LEFT_ANTENNA].cr_r = (RotateX(0)*RotateY(0)*RotateZ(-45));
    
    //Eye R
    head.child[RIGHT_EYE].color_index = 1;
    head.child[RIGHT_EYE].cr_t = Translate(-0.3, 0.5, 0.1);
    head.child[RIGHT_EYE].cr_s = Scale(0.15, 1.5, 0.15);
    head.child[RIGHT_EYE].pivot = Translate(0.0, 0.0, 0.0);
    head.child[RIGHT_EYE].cr_r = (RotateX(90)*RotateY(0)*RotateZ(0 ));
    
    //Eye L
    head.child[LEFT_EYE].color_index = 1;
    head.child[LEFT_EYE].cr_t = Translate(0.3, 0.5, 0.1);
    head.child[LEFT_EYE].cr_s = Scale(0.15, 1.5, 0.15);
    head.child[LEFT_EYE].pivot = Translate(0.0, 0.0, 0.0);
    head.child[LEFT_EYE].cr_r = (RotateX(90)*RotateY(0)*RotateZ(0 ));
    
    //Body
    limbs[BODY].color_index = 0;
    limbs[BODY].cr_t = Translate(0.0, 3.0, 0.0);
    limbs[BODY].cr_s = Scale(2.0, 2.0, 2.0);
    limbs[BODY].pivot = Translate(0.0, 0.0, 0.0);
    limbs[BODY].parentcrm = origin;
    
    //Arm R
    limbs[RIGHT_ARM].color_index = 0;
    limbs[RIGHT_ARM].cr_t = Translate(-1.35, 3.75, 0.0);
    limbs[RIGHT_ARM].cr_s = Scale(0.75, 1.75, 0.75);
    limbs[RIGHT_ARM].pivot = Translate(0.0, -0.8, 0.0);
    limbs[RIGHT_ARM].parentcrm = origin;
    
    //Shoulder R
    joints[RIGHT_SHOULDER].color_index = 0;
    joints[RIGHT_SHOULDER].cr_t = Translate(0.0, .65, 0.0);
    joints[RIGHT_SHOULDER].cr_s = Scale(0.375, 0.375, 0.375);
    joints[RIGHT_SHOULDER].pivot = Translate(0.0, -0.65, 0.0);
    joints[RIGHT_SHOULDER].parentcrm = limbs[RIGHT_ARM].cr_t * limbs[RIGHT_ARM].cr_r;
    
    //Upper Leg R
    limbs[RIGHT_UPPER_LEG].color_index = 0;
    limbs[RIGHT_UPPER_LEG].cr_t = Translate(-0.5, 2.0, 0.0);
    limbs[RIGHT_UPPER_LEG].cr_s = Scale(0.75, 1.0, 0.75);
    limbs[RIGHT_UPPER_LEG].pivot = Translate(0.0, -0.5, 0.0);
    limbs[RIGHT_UPPER_LEG].parentcrm = origin;
    limbs[RIGHT_UPPER_LEG].child = RIGHT_LOWER_LEG;
    
    //Lower Leg R
    limbs[RIGHT_LOWER_LEG].color_index = 0;
    limbs[RIGHT_LOWER_LEG].cr_t = Translate(0.0, -1.0, 0.0);
    limbs[RIGHT_LOWER_LEG].cr_s = Scale(0.75, 1.0, 0.75);
    limbs[RIGHT_LOWER_LEG].pivot = Translate(0.0, -0.5, 0.0);
    limbs[RIGHT_LOWER_LEG].parentcrm = limbs[RIGHT_UPPER_LEG].cr_t * limbs[RIGHT_UPPER_LEG].cr_r;
    
    //Knee R
    joints[RIGHT_KNEE].color_index = 0;
    joints[RIGHT_KNEE].cr_t = Translate(0.0, -0.5, 0.0);
    joints[RIGHT_KNEE].cr_s = Scale(0.375, 0.375, 0.375);
    joints[RIGHT_KNEE].pivot = Translate(0.0, -0.50, 0.0);
    joints[RIGHT_KNEE].parentcrm = limbs[RIGHT_UPPER_LEG].cr_t * limbs[RIGHT_UPPER_LEG].cr_r;
    
    //Arm L
    limbs[LEFT_ARM].color_index = 0;
    limbs[LEFT_ARM].cr_t = Translate(1.35, 3.75, 0.0);
    limbs[LEFT_ARM].cr_s = Scale(0.75, 1.75, 0.75);
    limbs[LEFT_ARM].pivot = Translate(0.0, -0.8, 0.0);
    limbs[LEFT_ARM].parentcrm = origin;
    
    //Shoulder R
    joints[LEFT_SHOULDER].color_index = 0;
    joints[LEFT_SHOULDER].cr_t = Translate(0.0, 0.65, 0.0);
    joints[LEFT_SHOULDER].cr_s = Scale(0.375, 0.375, 0.375);
    joints[LEFT_SHOULDER].pivot = Translate(0.0, -0.65, 0.0);
    joints[LEFT_SHOULDER].parentcrm = limbs[LEFT_ARM].cr_t * limbs[LEFT_ARM].cr_r;
    
    //Upper Leg L
    limbs[LEFT_UPPER_LEG].color_index = 0;
    limbs[LEFT_UPPER_LEG].cr_t = Translate(0.5, 2.0, 0.0);
    limbs[LEFT_UPPER_LEG].cr_s = Scale(0.75, 1.0, 0.75);
    limbs[LEFT_UPPER_LEG].pivot = Translate(0.0, -0.5, 0.0);
    limbs[LEFT_UPPER_LEG].parentcrm = origin;
    limbs[LEFT_UPPER_LEG].child = LEFT_LOWER_LEG;
    //Knee L
    joints[LEFT_KNEE].color_index = 0;
    joints[LEFT_KNEE].cr_t = Translate(0.0, -0.5, 0.0);
    joints[LEFT_KNEE].cr_s = Scale(0.375, 0.375, 0.375);
    joints[LEFT_KNEE].pivot = Translate(0.0, -0.50, 0.0);
    joints[LEFT_KNEE].parentcrm = limbs[LEFT_LOWER_LEG].cr_t * limbs[LEFT_LOWER_LEG].cr_r;
    //Lower Leg L
    limbs[LEFT_LOWER_LEG].color_index = 0;
    limbs[LEFT_LOWER_LEG].cr_t = Translate(0.0, -1.0, 0.0);
    limbs[LEFT_LOWER_LEG].cr_s = Scale(0.75, 1.0, 0.75);
    limbs[LEFT_LOWER_LEG].pivot = Translate(0.0, -0.5, 0.0);
    limbs[LEFT_LOWER_LEG].parentcrm = limbs[LEFT_UPPER_LEG].cr_t * limbs[LEFT_UPPER_LEG].cr_r;
    
    //GROUND
    limbs[GROUND].color_index = 1;
    limbs[GROUND].cr_t = Translate(0.0, -1, 0.0);
    limbs[GROUND].cr_s = Scale(100.0, 1.0, 100.0);
    limbs[GROUND].pivot = Translate(0.0, -1.0, 0.0);
    limbs[GROUND].parentcrm = origin;
    
    for (int i = 0; i < 9; i++) {
        limbs[i].material_ambient = { 0.6, 0.6, 0.6, 1.0 };
        limbs[i].material_ambient *= vertex_colors[limbs[i].color_index];
        limbs[i].material_diffuse = { 0.5, 0.5, 0.5, 1.0 };
        limbs[i].material_diffuse *= vertex_colors[limbs[i].color_index];
        limbs[i].material_specular = { 1.0, 1.0, 1.0, 1.0 };
        limbs[i].material_specular *= vertex_colors[limbs[i].color_index];
        limbs[i].cr_r = (RotateX(0)*RotateY(0)*RotateZ(0 ));
        limbs[i].crm = limbs[i].parentcrm * limbs[i].cr_t * (limbs[i].cr_r*limbs[i].pivot) * limbs[i].cr_s;
    }
    
    head.material_ambient = { 0.6, 0.6, 0.6, 1.0 };
    head.material_ambient *= vertex_colors[head.color_index];
    head.material_diffuse = { 0.9, 0.9, 0.9, 1.0 };
    head.material_diffuse *= vertex_colors[head.color_index];
    head.material_specular = { 1.0, 1.0, 1.0, 1.0 };
    head.material_specular *= vertex_colors[head.color_index];
    head.cr_r = (RotateX(0)*RotateY(0)*RotateZ(0 ));
    head.crm = head.parentcrm * head.cr_t * (head.cr_r*head.pivot) * head.cr_s;
    
    for (int i = 0; i < 4; i++) {
        head.child[i].material_ambient = { 0.6, 0.6, 0.6, 1.0 };
        head.child[i].material_ambient *= vertex_colors[head.child[i].color_index];
        head.child[i].material_diffuse = { 0.5, 0.5, 0.5, 1.0 };
        head.child[i].material_diffuse *= vertex_colors[head.child[i].color_index];
        head.child[i].material_specular = { 1.0, 1.0, 1.0, 1.0 };
        head.child[i].material_specular *= vertex_colors[head.child[i].color_index];
        head.child[i].parentcrm = head.cr_t * head.cr_r;
        head.child[i].crm = head.child[i].parentcrm * head.child[i].cr_t * (head.child[i].cr_r*head.child[i].pivot) * head.child[i].cr_s;
        
        joints[i].material_ambient = { 0.6, 0.6, 0.6, 1.0 };
        joints[i].material_ambient *= vertex_colors[joints[i].color_index];
        joints[i].material_diffuse = { 0.5, 0.5, 0.5, 1.0 };
        joints[i].material_diffuse *= vertex_colors[joints[i].color_index];
        joints[i].material_specular = { 1.0, 1.0, 1.0, 1.0 };
        joints[i].material_specular *= vertex_colors[joints[i].color_index];
        joints[i].crm = joints[i].parentcrm * joints[i].cr_t * (joints[i].cr_r*joints[i].pivot) * joints[i].cr_s;
    }
}

void Robot::Draw() {
    int k = 0;
    
    for (int i = 0; i < 9; i++)
    {
        if (cubes_to_rotate[k] == HEAD) {
            mat4  m = ( RotateX( Theta[Xaxis] ) * RotateY( Theta[Yaxis] ) * RotateZ( Theta[Zaxis] ) );
            head.cr_r = m * head.cr_r;
            head.crm = head.parentcrm * head.cr_t * (head.cr_r*head.pivot) * head.cr_s;
            for (int i = 0; i < 4; i++) {
                head.child[i].crm = head.child[i].parentcrm * head.child[i].cr_t * (head.child[i].cr_r*head.child[i].pivot) * head.child[i].cr_s;
                head.child[i].parentcrm =  head.cr_t * head.cr_r;
            }
            k++;
        }
        if ( i == cubes_to_rotate[k]){
            k++;
            mat4  m = ( RotateX( Theta[Xaxis] ) * RotateY( Theta[Yaxis] ) * RotateZ( Theta[Zaxis] ) );
            limbs[i].cr_r = m * limbs[i].cr_r;
            limbs[i].crm = limbs[i].parentcrm * limbs[i].cr_t * (limbs[i].cr_r*limbs[i].pivot) * limbs[i].cr_s;
        }
        head.Draw();
        if (i < 5) {
            head.child[i].Draw();
            joints[i].Draw();
        }
        limbs[i].Draw();
        if (limbs[i].child != 10) {
            int j = limbs[i].child;
            limbs[j].crm = limbs[j].parentcrm * limbs[j].cr_t * (limbs[j].cr_r*limbs[j].pivot) * limbs[j].cr_s;
            limbs[j].parentcrm =  limbs[i].cr_t * limbs[i].cr_r;
            joints[RIGHT_KNEE].crm = joints[RIGHT_KNEE].parentcrm * joints[RIGHT_KNEE].cr_t * (joints[RIGHT_KNEE].cr_r*joints[RIGHT_KNEE].pivot) * joints[RIGHT_KNEE].cr_s;
            joints[RIGHT_KNEE].parentcrm =  limbs[RIGHT_UPPER_LEG].cr_t * limbs[RIGHT_UPPER_LEG].cr_r;
            joints[LEFT_KNEE].crm = joints[LEFT_KNEE].parentcrm * joints[LEFT_KNEE].cr_t * (joints[LEFT_KNEE].cr_r*joints[LEFT_KNEE].pivot) * joints[LEFT_KNEE].cr_s;
            joints[LEFT_KNEE].parentcrm =  limbs[LEFT_UPPER_LEG].cr_t * limbs[LEFT_UPPER_LEG].cr_r;
        }
    }
    usleep(5000);
}

Robot droid;

void init() {
    //Init Shapes
    cube();
    sphere(5);
    cylinder();
    
    //Load Shaders
    GLuint program = InitShader( "/Users/evan.jarrett575/Dropbox/workspace/example1/vshader42.glsl",
                                "/Users/evan.jarrett575/Dropbox/workspace/example1/fshader42.glsl" );
    glUseProgram( program );
    
    //Create a vertex array object
    GLuint vao;
    glGenVertexArraysAPPLE( 1, &vao );
    glBindVertexArrayAPPLE( vao );
    
    //Create buffer object and transfer vertex attribute data
    GLuint buffer;
    glGenBuffers( 1, &buffer );
    glBindBuffer( GL_ARRAY_BUFFER, buffer );
    glBufferData( GL_ARRAY_BUFFER, sizeof(points) + sizeof(normals) + sizeof(sdata) + sizeof(snormals)+ sizeof(cdata) + sizeof(cnormals), NULL, GL_STATIC_DRAW );
    glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof(points), points );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(points), sizeof(normals), normals );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(points) + sizeof(normals), sizeof(sdata), sdata );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(points) + sizeof(normals) + sizeof(sdata), sizeof(snormals), snormals );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(points) + sizeof(normals) + sizeof(sdata)+ sizeof(snormals), sizeof(cdata), cdata );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(points) + sizeof(normals) + sizeof(sdata)+ sizeof(snormals)+ sizeof(cdata), sizeof(cnormals), cnormals );
    
    //Set up vertex arrays
    vPosition = glGetAttribLocation( program, "vPosition" );
    glEnableVertexAttribArray( vPosition );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
    
    vNormal = glGetAttribLocation( program, "vNormal" );
    glEnableVertexAttribArray( vNormal );
    glVertexAttribPointer( vNormal, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(points)) );
    
    
    //Setup uniform variables for lighting
    ka = glGetUniformLocation(program, "ka");
    kd = glGetUniformLocation(program, "kd");
    ks = glGetUniformLocation(program, "ks");

    point4 light_position( 10.0, 5.0, 0.0, 0.0 );
    color4 light_ambient( 1.0, 1.0, 1.0, 1.0 );
    color4 light_diffuse( 1.0, 1.0, 1.0, 1.0 );
    color4 light_specular( 1.0, 1.0, 1.0, 1.0 );
    float  material_shininess = 5.0;

    glUniform4fv( glGetUniformLocation(program, "AmbientLight"), 1, light_ambient );
    glUniform4fv( glGetUniformLocation(program, "DiffuseLight"), 1, light_diffuse );
    glUniform4fv( glGetUniformLocation(program, "SpecularLight"), 1, light_specular );
    glUniform4fv( glGetUniformLocation(program, "LightPosition"), 1, light_position );
    glUniform1f( glGetUniformLocation(program, "Shininess"), material_shininess );
    
    //Setup uniform variables for transformations
    vColor = glGetUniformLocation( program, "vColor" );
    model = glGetUniformLocation( program, "model" );
    view = glGetUniformLocation( program, "view" );
    projection = glGetUniformLocation( program, "projection" );
    
    glEnable( GL_DEPTH_TEST );
    glClearColor( 0.4, 0.5, 0.7, 1.0 );
}

void display( void ) {
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    //View
    mat4  v = LookAt( eye, at, up );
    glUniformMatrix4fv( view, 1, GL_TRUE, v );
    
    //Projection
    mat4  p = Frustum( left, right, bottom, top, zNear, zFar );
    glUniformMatrix4fv( projection, 1, GL_TRUE, p );
    
    //Draw Android
    droid.Draw();
    glutSwapBuffers();
}

void idle( void ) {
    if ( abs(theta_total) < 90.0 && do_rotation == true ) {
        Theta[Axis] = dirn*0.5;
        theta_total += Theta[Axis];   
    }
    else {
        theta_total = 0.0;
        Theta[Axis] = 0.0;
        do_rotation = false;
    }
    glutPostRedisplay();
}

void reshape( int width, int height ) {
    glViewport( 0, 0, width, height );
}

void keyboard( unsigned char key, int x, int y ) {
    
    int cubes_to_rotate[4];
    GLfloat Cdist = 3.0;
    switch( key )
    {
        case 033: // Escape Key
        case '`':
            exit( EXIT_SUCCESS );
            break;
            
            //----- cameras
        case '1':
            Cx = 3.0; Cy = 3.0; Cz = 3.0*Cdist;
            eye = { Cx, Cy, Cz, 1.0 };
            up = { 0.0, 1.0, 0.0, 0.0 };
            zNear = 1.5, zFar = 50.0;
            break;
        case '2':
            Cx = 5.0; Cy = 5.0; Cz = -5.0*Cdist;
            eye = { Cx, Cy, Cz, 1.0 };
            up = { 0.0, 1.0, 0.0, 0.0 };
            zNear = 1.5, zFar = 50.0;
            break;
        case '3':
            Cx = 5.0*Cdist; Cy = 5.0; Cz = 5.0;
            eye = { Cx, Cy, Cz, 1.0 };
            up = { 0.0, 1.0, 0.0, 0.0 };
            zNear = 1.5, zFar = 50.0;
            break;
        case '4':
            Cx = -5.0*Cdist; Cy = 5.0; Cz = 5.0;
            eye = { Cx, Cy, Cz, 1.0 };
            up = { 0.0, 1.0, 0.0, 0.0 };
            zNear = 1.5, zFar = 50.0;
            break;
        case '5':
            Cx = 5.0; Cy = 5.0*Cdist; Cz = 5.0;
            eye = { Cx, Cy, Cz, 1.0 };
            up = { 0.0, 0.0, -1.0, 0.0 };
            zNear = 1.5, zFar = 50.0;
            break;
        case '6':
            Cx = 3.0*Cdist; Cy = 3.0*Cdist; Cz = 3.0*Cdist;
            eye = { Cx, Cy, Cz, 1.0 };
            up = { 0.0, 1.0, 0.0, 0.0 };
            zNear = 1.5, zFar = 50.0;
            break;
        case 'j':
            Cx += 0.1;
            eye = { Cx, Cy, Cz, 1.0 };
            break;
        case 'k':
            Cx -= 0.1;
            eye = { Cx, Cy, Cz, 1.0 };
            break;
        case 'q'://RIGHT ARM UP
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = -1;
                cubes_to_rotate[0]= RIGHT_ARM;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'Q'://RIGHT ARM DOWN
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = 1;
                cubes_to_rotate[0]= RIGHT_ARM;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'w'://TURN HEAD LEFT
            if (!do_rotation) {
                do_rotation = true;
                Axis = Yaxis;
                Theta[Axis] = 0.0;
                dirn = -1;
                cubes_to_rotate[0]= HEAD;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'W'://TURN HEAD RIGHT
            if (!do_rotation) {
                do_rotation = true;
                Axis = Yaxis;
                Theta[Axis] = 0.0;
                dirn = 1;
                cubes_to_rotate[0]= HEAD;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'e'://LEFT ARM UP
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = -1;
                cubes_to_rotate[0]= LEFT_ARM;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'E'://LEFT ARM DOWN
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = 1;
                cubes_to_rotate[0]= LEFT_ARM;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'd'://LEFT UPPER LEG UP
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = -1;
                cubes_to_rotate[0]= LEFT_UPPER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'D'://LEFT UPPER LEG DOWN
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = 1;
                cubes_to_rotate[0]= LEFT_UPPER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'c'://LEFT LOWER LEG UP
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = -1;
                cubes_to_rotate[0]= LEFT_LOWER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'C'://LEFT LOWER LEG DOWN
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = 1;
                cubes_to_rotate[0]= LEFT_LOWER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'a'://LEFT UPPER LEG UP
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = -1;
                cubes_to_rotate[0]= RIGHT_UPPER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'A'://LEFT UPPER LEG DOWN
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = 1;
                cubes_to_rotate[0]= RIGHT_UPPER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'z'://LEFT LOWER LEG UP
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = -1;
                cubes_to_rotate[0]= RIGHT_LOWER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'Z'://LEFT LOWER LEG DOWN
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = 1;
                cubes_to_rotate[0]= RIGHT_LOWER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
    }
    
    glutPostRedisplay();
}

int main( int argc, char **argv ) {
    glutInit( &argc, argv );
    glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
    glutInitWindowSize( 1280, 1024 );
    glutCreateWindow( "Android" );
    
    init();
    
    glutDisplayFunc( display );
    glutKeyboardFunc( keyboard );
    glutIdleFunc( idle );
    glutReshapeFunc( reshape );
    
    glutMainLoop();
    return 0;
}