#include "Angel.h"
#include "Robot.h"
#include <unistd.h>
#include <iostream>

typedef Angel::vec4  color4;
typedef Angel::vec4  point4;

// Sphere
//===========================================================================

point4 sdata[NumSphereVertices];
vec3 snormals[NumSphereVertices];
GLsizei w=512, h=512; //???
int sphereIndex = 0;

void triangle(const point4& a, const point4& b, const point4& c ) {    
    snormals[sphereIndex] = vec3(a.x,a.y,a.z);
    sdata[sphereIndex] = a;  sphereIndex++;
    snormals[sphereIndex] = vec3(b.x,b.y,b.z);
    sdata[sphereIndex] = b;  sphereIndex++;
    snormals[sphereIndex] = vec3(c.x,c.y,c.z);
    sdata[sphereIndex] = c;  sphereIndex++;
}

point4 unit( const point4& p ) {
    float len = p.x*p.x + p.y*p.y + p.z*p.z;
    
    point4 t;
    if ( len > DivideByZeroTolerance ) {
        t = p / sqrt(len);
        t.w = 1.0;
    }
    return t;
}

void divide_triangle( const point4& a, const point4& b, const point4& c, int count ) {
    if ( count > 0 ) {
        point4 v1 = unit( a + b );
        point4 v2 = unit( a + c );
        point4 v3 = unit( b + c );
        divide_triangle(  a, v1, v2, count - 1 );
        divide_triangle(  c, v2, v3, count - 1 );
        divide_triangle(  b, v3, v1, count - 1 );
        divide_triangle( v1, v3, v2, count - 1 );
    }
    else {
        triangle( a, b, c );
    }
}

void sphere( int sub_divides) {

    point4 v[4] = {
        vec4( 0.0, 0.0, 1.0, 1.0 ),
        vec4( 0.0, 0.942809, -0.333333, 1.0 ),
        vec4( -0.816497, -0.471405, -0.333333, 1.0 ),
        vec4( 0.816497, -0.471405, -0.333333, 1.0 )
    };
    
    divide_triangle( v[0], v[1], v[2], sub_divides );
    divide_triangle( v[3], v[2], v[1], sub_divides );
    divide_triangle( v[0], v[3], v[1], sub_divides );
    divide_triangle( v[0], v[2], v[3], sub_divides );
}

//Cube
//===========================================================================

point4 points[NumVertices];
vec3   normals[NumVertices];
int cubeIndex = 0;

void quad( int a, int b, int c, int d) {
    // Vertices of a unit cube centered at origin, sides aligned with axes
    point4 vertices[8] = {
        point4( -0.5, -0.5,  0.5, 1.0 ),
        point4( -0.5,  0.5,  0.5, 1.0 ),
        point4(  0.5,  0.5,  0.5, 1.0 ),
        point4(  0.5, -0.5,  0.5, 1.0 ),
        point4( -0.5, -0.5, -0.5, 1.0 ),
        point4( -0.5,  0.5, -0.5, 1.0 ),
        point4(  0.5,  0.5, -0.5, 1.0 ),
        point4(  0.5, -0.5, -0.5, 1.0 )
    };
    // Initialize temporary vectors along the quad's edge to compute its face normal
    vec4 u = vertices[b] - vertices[a];
    vec4 v = vertices[c] - vertices[b];
    
    vec3 normal = normalize( cross(u, v) );
    
    normals[cubeIndex] = normal; points[cubeIndex] = vertices[a]; cubeIndex++;
    normals[cubeIndex] = normal; points[cubeIndex] = vertices[b]; cubeIndex++;
    normals[cubeIndex] = normal; points[cubeIndex] = vertices[c]; cubeIndex++;
    normals[cubeIndex] = normal; points[cubeIndex] = vertices[a]; cubeIndex++;
    normals[cubeIndex] = normal; points[cubeIndex] = vertices[c]; cubeIndex++;
    normals[cubeIndex] = normal; points[cubeIndex] = vertices[d]; cubeIndex++;
}

void cube() {
    quad( 1, 0, 3, 2 );
    quad( 2, 3, 7, 6 );
    quad( 3, 0, 4, 7 );
    quad( 6, 5, 1, 2 );
    quad( 4, 5, 6, 7 );
    quad( 5, 4, 0, 1 );
}

//Cylinder
//===========================================================================

vec3 cnormals[NumSides * 12];
point4 cdata[NumSides * 12];
point4 cylverts[NumSides * 4];
int cylIndex = 0;

void cyl_triangles(const point4& a, const point4& b, const point4& c ) {
    vec4 u = b - a;
    vec4 v = c - b;
    
    vec3 normal = normalize( cross(u, v) );
    
    vec3 normal_a = vec3(a.x,normal.y,a.z);
    vec3 normal_b = vec3(b.x,normal.y,b.z);
    vec3 normal_c = vec3(c.x,normal.y,c.z);
    
    cnormals[cylIndex] = normal_a; cdata[cylIndex] = a;  cylIndex++;
    cnormals[cylIndex] = normal_b; cdata[cylIndex] = b;  cylIndex++;
    cnormals[cylIndex] = normal_c; cdata[cylIndex] = c;  cylIndex++;
}

void cylSides(const point4& a, const point4& b, const point4& c, const point4& d) {
    cyl_triangles( a, b, c);
    cyl_triangles( a, c, d);

}

void cylinder () {
    vec4 center1 = vec4(0.0, -0.5, 0.0, 1.0);
    vec4 center2 = vec4(0.0, 0.5, 0.0, 1.0);
    int i,k=0;
    GLfloat frac = ((2*M_PI)/NumSides);
    for (i=0; i < NumSides; i++) {
        cylverts[i] = vec4(.5*(cos(frac*i)), -0.5, .5*(sin(frac*i)), 1.0);
        cylverts[i+NumSides] = vec4(.5*(cos(frac*i)), 0.5, .5*(sin(frac*i)), 1.0);
    }
    for (k=0; k < NumSides; k++) {
        int l = k+1;
        if (l >= NumSides) {
            l -= NumSides;
        }
        cyl_triangles(cylverts[k], cylverts[l], center1);
        cyl_triangles(cylverts[k+NumSides], cylverts[l+NumSides], center2);
        cylSides(cylverts[k],cylverts[l],cylverts[l+NumSides], cylverts[k+NumSides]);
    }
}