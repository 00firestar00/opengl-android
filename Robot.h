#ifndef __ROBOT_H__
#define __ROBOT_H__

#include "Angel.h"
#include <unistd.h>
#include <iostream>

#define NumTimesToSubdivide 5
//(4 faces)^(NumTimesToSubdivide + 1);  5  -> 4096  or 2 -> 64
#define NumTriangles 4096
//3 * NumTriangles
#define NumSphereVertices 12288
//(6 faces)(2 triangles/face)(3 vertices/triangle)
#define NumVertices 36
#define NumSides 100

enum RobotPart { GROUND, RIGHT_ARM, LEFT_ARM, RIGHT_UPPER_LEG, LEFT_UPPER_LEG, RIGHT_LOWER_LEG, LEFT_LOWER_LEG, BODY, HEAD };
enum Joints { LEFT_SHOULDER, RIGHT_SHOULDER, LEFT_KNEE, RIGHT_KNEE};
enum HeadPart {LEFT_ANTENNA, RIGHT_ANTENNA, LEFT_EYE, RIGHT_EYE};
enum { Xaxis, Yaxis, Zaxis, NumAxes };

typedef Angel::vec4  color4;
typedef Angel::vec4  point4;

////From Main
//extern GLuint ka, kd, ks;
//extern int Axis;
//extern GLfloat Theta[NumAxes];
//extern GLfloat theta_total;
//extern int dirn;
//extern bool do_rotation;
//extern GLuint model, vColor, vPosition, vNormal;
//extern GLfloat left, right, bottom, top, zNear, zFar;
//extern GLfloat Cx, Cy, Cz;
//extern point4 eye;
//extern point4 at;
//extern vec4 up;

//From DrawShapes
extern point4 sdata[NumSphereVertices];
extern vec3 snormals[NumSphereVertices];
extern GLsizei w, h; //???
extern int sphereIndex, cubeIndex, cylIndex;
extern point4 points[NumVertices];
extern vec3 normals[NumVertices];
extern vec3 cnormals[NumSides * 12];
extern point4 cdata[NumSides * 12];
extern point4 cylverts[NumSides * 4];

//Global Functions
void triangle(vec3 normals[], point4 data[], const point4& a, const point4& b, const point4& c);
point4 unit(const point4& p);
void divide_triangle(vec3 normals[], point4 data[], const point4& a, const point4& b, const point4& c, int count);
void sphere(int sub_divides);
void quad(int a, int b, int c, int d);
void cube();
void cyl_triangles(const point4& a, const point4& b, const point4& c);
void cylSides(int a, int b, int c, int d);
void cylinder();

#endif