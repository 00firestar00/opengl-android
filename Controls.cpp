#include "Angel.h"
#include "Robot.h"
#include <unistd.h>
#include <iostream>

typedef Angel::vec4  color4;
typedef Angel::vec4  point4;

void keyboard( unsigned char key, int x, int y ) {
    
    getDroid();
    
    int cubes_to_rotate[4];
    GLfloat Cdist = 3.0;
    switch( key )
    {
        case 033: // Escape Key
        case '`':
            exit( EXIT_SUCCESS );
            break;
            
            //----- cameras
        case '1':
            Cx = 2.0; Cy = 2.0; Cz = 2.0*Cdist;
            eye = { Cx, Cy, Cz, 1.0 };
            up = { 0.0, 1.0, 0.0, 0.0 };
            break;
        case '2':
            Cx = 2.0; Cy = 2.0; Cz = -2.0*Cdist;
            eye = { Cx, Cy, Cz, 1.0 };
            up = { 0.0, 1.0, 0.0, 0.0 };
            break;
        case '3':
            Cx = 2.0*Cdist; Cy = 2.0; Cz = 2.0;
            eye = { Cx, Cy, Cz, 1.0 };
            up = { 0.0, 1.0, 0.0, 0.0 };
            break;
        case '4':
            Cx = -2.0*Cdist; Cy = 2.0; Cz = 2.0;
            eye = { Cx, Cy, Cz, 1.0 };
            up = { 0.0, 1.0, 0.0, 0.0 };
            break;
        case '5':
            Cx = 2.0; Cy = 2.0*Cdist; Cz = 2.0;
            eye = { Cx, Cy, Cz, 1.0 };
            up = { 0.0, 0.0, -1.0, 0.0 };
            break;
        case '6':
            Cx = 2.0; Cy = -2.0*Cdist; Cz = 2.0;
            eye = { Cx, Cy, Cz, 1.0 };
            up = { 0.0, 0.0, 1.0, 0.0 };
            break;
        case '7':
            Cx = 2.0*Cdist; Cy = 2.0*Cdist; Cz = 2.0*Cdist;
            eye = { Cx, Cy, Cz, 1.0 };
            up = { 0.0, 1.0, 0.0, 0.0 };
            zNear = 1.0*1.5, zFar = 50.0;
            break;
        case 'j':
            Cx += 0.1;
            eye = { Cx, Cy, Cz, 1.0 };
            break;
        case 'k':
            Cx -= 0.1;
            eye = { Cx, Cy, Cz, 1.0 };
            break;
        case 'q'://RIGHT ARM UP
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = -1;
                cubes_to_rotate[0]= RIGHT_ARM;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'Q'://RIGHT ARM DOWN
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = 1;
                cubes_to_rotate[0]= RIGHT_ARM;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'w'://TURN HEAD LEFT
            if (!do_rotation) {
                do_rotation = true;
                Axis = Yaxis;
                Theta[Axis] = 0.0;
                dirn = -1;
                cubes_to_rotate[0]= HEAD;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'W'://TURN HEAD RIGHT
            if (!do_rotation) {
                do_rotation = true;
                Axis = Yaxis;
                Theta[Axis] = 0.0;
                dirn = 1;
                cubes_to_rotate[0]= HEAD;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'e'://LEFT ARM UP
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = -1;
                cubes_to_rotate[0]= LEFT_ARM;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'E'://LEFT ARM DOWN
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = 1;
                cubes_to_rotate[0]= LEFT_ARM;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'd'://LEFT UPPER LEG UP
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = -1;
                cubes_to_rotate[0]= LEFT_UPPER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'D'://LEFT UPPER LEG DOWN
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = 1;
                cubes_to_rotate[0]= LEFT_UPPER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'c'://LEFT LOWER LEG UP
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = -1;
                cubes_to_rotate[0]= LEFT_LOWER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'C'://LEFT LOWER LEG DOWN
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = 1;
                cubes_to_rotate[0]= LEFT_LOWER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'a'://LEFT UPPER LEG UP
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = -1;
                cubes_to_rotate[0]= RIGHT_UPPER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'A'://LEFT UPPER LEG DOWN
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = 1;
                cubes_to_rotate[0]= RIGHT_UPPER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'z'://LEFT LOWER LEG UP
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = -1;
                cubes_to_rotate[0]= RIGHT_LOWER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
        case 'Z'://LEFT LOWER LEG DOWN
            if (!do_rotation) {
                do_rotation = true;
                Axis = Xaxis;
                Theta[Axis] = 0.0;
                dirn = 1;
                cubes_to_rotate[0]= RIGHT_LOWER_LEG;
                droid.SetCubesRotate(cubes_to_rotate);
            }
            break;
    }
    
    glutPostRedisplay();
}
